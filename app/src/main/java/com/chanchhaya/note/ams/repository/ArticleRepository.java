package com.chanchhaya.note.ams.repository;

import androidx.lifecycle.MutableLiveData;

import com.chanchhaya.note.ams.api.response.BaseResponse;
import com.chanchhaya.note.ams.model.Article;
import com.chanchhaya.note.ams.model.Pagination;

import java.util.List;

public interface ArticleRepository {

    MutableLiveData<BaseResponse<List<Article>>> findAll(Pagination pagination);

}
