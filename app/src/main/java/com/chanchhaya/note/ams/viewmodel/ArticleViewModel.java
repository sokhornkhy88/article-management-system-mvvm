package com.chanchhaya.note.ams.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chanchhaya.note.ams.api.response.BaseResponse;
import com.chanchhaya.note.ams.model.Article;
import com.chanchhaya.note.ams.model.Pagination;
import com.chanchhaya.note.ams.repository.impl.ArticleRepositoryImpl;

import java.util.List;

public class ArticleViewModel extends ViewModel {

    private ArticleRepositoryImpl articleRepository;
    private MutableLiveData<BaseResponse<List<Article>>> data = new MutableLiveData<>();

    public void init() {
        this.articleRepository = new ArticleRepositoryImpl();
    }

    public MutableLiveData<BaseResponse<List<Article>>> getArticleLiveData(Pagination pagination) {
        return articleRepository.findAll(pagination);
    }



}
