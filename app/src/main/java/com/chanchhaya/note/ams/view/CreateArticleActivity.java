package com.chanchhaya.note.ams.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.chanchhaya.note.ams.R;
import com.chanchhaya.note.ams.databinding.ActivityActiclesListBinding;
import com.chanchhaya.note.ams.databinding.ActivityCreateArticleBinding;

public class CreateArticleActivity extends AppCompatActivity {

    ActivityCreateArticleBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCreateArticleBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }
}