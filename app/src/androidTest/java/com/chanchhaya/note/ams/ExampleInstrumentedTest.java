package com.chanchhaya.note.ams;

import android.content.Context;
import android.util.Log;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.chanchhaya.note.ams.api.ArticleService;
import com.chanchhaya.note.ams.api.config.RetrofitConfig;
import com.chanchhaya.note.ams.api.response.BaseResponse;
import com.chanchhaya.note.ams.model.Article;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.chanchhaya.note.ams", appContext.getPackageName());
    }

    @Test
    public void findAllArticles() {
        ArticleService articleService =
                RetrofitConfig.createService(ArticleService.class);
        Call<BaseResponse<List<Article>>> articles = articleService.findAll();
        articles.enqueue(new Callback<BaseResponse<List<Article>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Article>>> call, Response<BaseResponse<List<Article>>> response) {
                if (response.isSuccessful()) {
                    Log.d("STATUS", "" + response.body());
                } else {
                    Log.d("STATUS", "" + response.body());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Article>>> call, Throwable t) {
                Log.e("CHECK", t.getLocalizedMessage());
                Log.d("STATUS", "Failed");
            }
        });
    }

    @Test
    public void findArticleById() {
        Log.d("TEST", "Hello Test 2");
    }
}