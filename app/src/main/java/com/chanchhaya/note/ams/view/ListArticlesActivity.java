package com.chanchhaya.note.ams.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chanchhaya.note.ams.adapter.article.ListArticleAdapter;
import com.chanchhaya.note.ams.api.response.BaseResponse;
import com.chanchhaya.note.ams.databinding.ActivityActiclesListBinding;
import com.chanchhaya.note.ams.model.Article;
import com.chanchhaya.note.ams.model.Pagination;
import com.chanchhaya.note.ams.utils.PaginationUtil;
import com.chanchhaya.note.ams.viewmodel.ArticleViewModel;

import java.util.ArrayList;
import java.util.List;

public class ListArticlesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ActivityActiclesListBinding binding;
    private ProgressBar progressBar;
    private ListArticleAdapter listArticleAdapter;
    private LinearLayoutManager linearLayoutManager;

    private boolean isReload = false;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    private Pagination pagination;

    private ArticleViewModel articleViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityActiclesListBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        configRecyclerView();

        binding.swipe.setOnRefreshListener(this);
        onFabClick();

        articleViewModel = new ViewModelProvider(this).get(ArticleViewModel.class);
        articleViewModel.init();

        getArticleLiveData();

    }

    private void onFabClick() {
        binding.fabCreateNew.setOnClickListener(v -> {
            Intent intent = new Intent(this, CreateArticleActivity.class);
            startActivity(intent);
        });
    }

    private void configRecyclerView() {

        pagination = new Pagination();

        linearLayoutManager = new LinearLayoutManager(this);
        listArticleAdapter = new ListArticleAdapter(this, new ArrayList<>());
        binding.rcvListArticle.setLayoutManager(linearLayoutManager);
        binding.rcvListArticle.setAdapter(listArticleAdapter);

        binding.rcvListArticle.addOnScrollListener(new PaginationUtil(linearLayoutManager, pagination.getLimit()) {

            @Override
            protected void loadMoreItems() {
                isLoading = true;
                pagination.increasePage();
                getArticleLiveData();
            }

            @Override
            public boolean isLastPage() {
                return pagination.getPage() == pagination.getTotalPages();
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

    }

    private void getArticleLiveData() {

        if (isLoading) {
            binding.progressBar.setVisibility(View.VISIBLE);
        }

        // view subscribes live data
        articleViewModel.getArticleLiveData(pagination).observe(this, new Observer<BaseResponse<List<Article>>>() {
            @Override
            public void onChanged(BaseResponse<List<Article>> listBaseResponse) {
                binding.progressBar.setVisibility(View.GONE);
                binding.swipe.setRefreshing(false);
                listArticleAdapter.setDataSet(listBaseResponse.getData());
                // successfully
                isReload = false;
                isLoading = false;
            }
        });
    }

    @Override
    public void onRefresh() {
        listArticleAdapter.getDataSet().clear();
        isReload = true;
        getArticleLiveData();
    }

}