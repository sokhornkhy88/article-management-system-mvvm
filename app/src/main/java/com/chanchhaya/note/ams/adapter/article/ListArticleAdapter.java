package com.chanchhaya.note.ams.adapter.article;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.chanchhaya.note.ams.R;
import com.chanchhaya.note.ams.model.Article;

import java.util.List;

public class ListArticleAdapter extends RecyclerView.Adapter<ListArticleAdapter.ArticleItemViewHolder> {

    private Context context;
    private List<Article> dataSet;
    private LayoutInflater inflater;

    public ListArticleAdapter(Context context, List<Article> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
        this.inflater = LayoutInflater.from(context);
    }

    public void setDataSet(List<Article> dataSet) {
        this.dataSet.addAll(dataSet);
        notifyDataSetChanged();
    }

    public List<Article> getDataSet() {
        return dataSet;
    }

    @NonNull
    @Override
    public ArticleItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.article_item_layout, parent, false);
        return new ArticleItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleItemViewHolder holder, int position) {
        holder.textTitle.setText(dataSet.get(position).getTitle());
        holder.textDescription.setText(dataSet.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    class ArticleItemViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView textTitle;
        private AppCompatTextView textDescription;

        public ArticleItemViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            textDescription = itemView.findViewById(R.id.text_description);
        }

    }

}
